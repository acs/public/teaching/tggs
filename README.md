# TGGS Special Block Lecture about Smart Grid Technology
by the [Intitute for Automation of Complex Power Systems](http://www.acs.eonerc.rwth-aachen.de/cms/~dlkd/E-ON-ERC-ACS/?lidx=1)

## Introduction to Modelica with OMNotebook

1. Double click on the Home folder icon
2. Navigate to the folder `gits/tggs/omnotebook_exercise`
3. Double click on `rlc - Student.onb`
4. Now, you can start completing the OMNotebook.

**Note**: Each model must be initialized by executing each cell with `shift+enter`

## Grid Modeling and Simulation - CIMverter, ModPowerSystems and OMEdit

**Note**: You can copy the commands with `ctrl+C` and paste them in the terminal with `ctrl+shift+V`

### CIMverter usage
Open a new Terminal and enter
```bash
cd ~/gits/CIMverter/build/bin
```
to change into the proper directory of the [CIMverter](http://fein-aachen.org/projects/cimverter/).

Then enter following command for the conversion of a sample grid in CIM to a Modelica system model file called `CIGRE.mo`:
```bash
./CIMverter -a ../../samples/CIGRE_MV_Rudion_With_LoadFlow_Results/ -o CIGRE
```
This CIGRE grid can be simulated by [OpenModelica](https://openmodelica.org/) based on the [ModPowerSystems](http://fein-aachen.org/projects/modpowersystems/) library:

### Simulation with OpenModelica
For simulating the generated `CIGRE.mo` grid:
1. Double click the `OMEdit` icon on the desktop
2. Choose `File -> Open Model/Library File(s)` from the menu
3. Starting from the home directory, choose the file `gits/modpowersystems/ModPowerSystems/package.mo`
4. Choose `File -> Open Model/Library File(s)` from the menu
5. Starting from the home directory, choose the generated file `gits/CIMverter/build/bin/CIGRE.mo`
6. Click on `CIGRÈ` in the left menu of OMEdit
7. Execute the simulation by choosing `Simulation -> Simulate` from the menu


### Further Insights

You can have a look into the CIM data with
```bash
cd ~/gits/CIMverter/samples/CIGRE_MV_Rudion_With_LoadFlow_Results/
mousepad Rootnet_FULL_NE_24J13h_EQ.xml
```

To see the templates which the CIMverter applies to obtain ModPowerSystems compatible models, go to
```bash
cd ~/gits/CIMverter/ModPowerSystems_templates/
```

You can view the system model template with
```bash
mousepad modelica.tpl
```

and a exemplary component model template with
```bash
mousepad Transformer.tpl
```



---

Last update: 24.08.2021
